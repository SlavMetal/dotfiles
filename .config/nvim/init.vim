call plug#begin()

" >>> Themes <<<
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Luxed/ayu-vim'

" >>> Other <<<
Plug 'sheerun/vim-polyglot'
Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'tpope/vim-fugitive'
Plug 'ajh17/VimCompletesMe'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }
Plug 'dense-analysis/ale'
Plug 'ellisonleao/glow.nvim', { 'branch': 'main' }

call plug#end()

""""""""""""""
" >>> UI <<< "
""""""""""""""
syntax enable

" NERDTree settings
map <C-n> :NERDTreeToggle %<CR>
" Close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
if (empty($TMUX))
  if (has("termguicolors"))
    set termguicolors
  endif
endif

" vim-indent-guides settings
let g:indent_guides_enable_on_vim_startup=1
let g:indent_guides_guide_size=1
let g:indent_guides_start_level=2
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#343c48 ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#343c48 ctermbg=4

" vim-airline settings
let g:airline_powerline_fonts=1

" ayu settings, also managed by ./lua/colors.lua
set background=dark
let g:airline_theme='ayu_mirage'
let g:ayucolor='mirage'
let g:ayu_extended_palette=1
colorscheme ayu

" Other
set number          " show line number
set cursorline      " highlight a current line
set wildmenu        " visual autocomplete for command menu
set showmatch       " highlight matching brace

""""""""""""""""
" >>> Tabs <<< "
""""""""""""""""
set tabstop=2       " number of visual spaces per TAB
set softtabstop=2   " number of spaces in tab when editing
set shiftwidth=2    " number of spaces to use for autoindent
set expandtab       " tabs are space
set autoindent
set copyindent      " copy indent from the previous line

""""""""""""""""""
" >>> Search <<< "
""""""""""""""""""
set incsearch       " search as characters are entered
set hlsearch        " highlight matches
set ignorecase      " ignore case when searching
set smartcase       " ignore case if search pattern is lower case, case-sensitive otherwise

"""""""""""""""""
" >>> Other <<< "
"""""""""""""""""
" Use system's clipboard by default
set clipboard+=unnamedplus
" Disable recording
map q <Nop>

""""""""""""""""""""
" >>> Keybinds <<< "
""""""""""""""""""""
" Switch between different windows by their direction
no <C-j> <C-w>j| "switching to below window 
no <C-k> <C-w>k| "switching to above window
no <C-l> <C-w>l| "switching to right window 
no <C-h> <C-w>h| "switching to left window

lua require('colors')
