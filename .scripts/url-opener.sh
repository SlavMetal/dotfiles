#!/bin/bash

# This script allows to choose a browser when opening URLs.
# In case no browsers are running, it will open the first whitelisted browser.
# In case one browser is running, a URL will be opened in it.
# In case more browsers are running, a user will be presented with a prompt to choose a browser.

declare -a whitelisted_browsers=("/usr/lib/firefox/firefox" "/usr/lib/firefox-developer-edition/firefox" "/usr/lib/chromium/chromium")
declare -a running_browsers=()

for i in "${whitelisted_browsers[@]}"
do
  if pidof "$i" >/dev/null
  then
    running_browsers+=("$i")
  fi
done

case "${#running_browsers[@]}" in
  0)
    ${whitelisted_browsers[0]} "$1" &
    ;;

  1)
    ${running_browsers[0]} "$1"
    ;;

  *)
    browser=$(echo "${running_browsers[@]}" | tr " " "\n" | fzf --preview "echo '$1'" --preview-window down:3:wrap)
    $browser "$1"
    ;;
esac
