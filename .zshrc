# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/max/.oh-my-zsh"

ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git gitignore sudo extract z colored-man-pages copyfile copydir dirhistory history-substring-search helm asdf aws)

source $ZSH/oh-my-zsh.sh

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# https://wiki.archlinux.org/title/GnuPG#Invalid_IPC_response_and_Inappropriate_ioctl_for_device
export GPG_TTY=$TTY

source ~/.config/zsh-config-private/aliases

# Config files
alias zshconf="nvim ~/.zshrc"
alias nvimconf="nvim ~/.config/nvim/init.vim"
alias i3conf="nvim ~/.config/i3/config"
# https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
alias config='/usr/bin/git --git-dir=$HOME/prs/me/dotfiles --work-tree=$HOME'

# System
alias p="yay"
alias upd="yay -Syu"
alias cleanpacman="paccache -rk 1; paccache -ruk0"
alias cleancache="~/.scripts/rmshit.py"
alias suroot='sudo -E -s' # Use oh-my-zsh with root user
alias wallsresize="mogrify -resize 2560\> ~/pix/walls/*"

#DevOps
alias tf="terraform"
alias k="kubecolor"
alias ktx="kubectx"
alias kns="kubens"
alias kg="kubecolor get"
alias kd="kubecolor describe"
alias klf="kubecolor logs -f"
alias kit='bash -xc '\''kubectl exec -it $0 -- /bin/bash'\'''
alias dit='bash -xc '\''docker exec -it $0 /bin/bash'\'''
alias bd='bash -xc '\''echo $0 | base64 -d'\'''
alias be='bash -xc '\''printf $0 | base64'\'''
alias dc="docker-compose"
alias printcrl="openssl crl -in - -text -noout"
alias printcert="openssl x509 -in - -text"
alias npm-groovy-lint="npm-groovy-lint --no-insight"
alias rmds='docker rm $(docker ps -qa)'
alias rmdr='docker ps | awk '{print $1}' | xargs -r docker kill'

# General
alias df="df -h"
alias free="free -h"
alias weather="curl wttr.in"
alias vim="nvim"
alias v="nvim"
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias clip='xclip -sel clip'
alias watch='watch '
alias wgu='sudo wg-quick up wg0'
alias wgd='sudo wg-quick down wg0'
alias ipaddr='curl https://ipecho.net/plain'

alias l='ls -lFh'     #size,show type,human readable
alias la='ls -lAFh'   #long list,show almost all,show type,human readable
alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable
alias ll='ls -l'      #long list
alias ldot='ls -ld .*'
alias lS='ls -1FSsh'
alias lx='exa -l'
alias lxa='exa -la'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_DEFAULT_OPTS="--border=rounded --pointer='|'"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:descriptions' format '[%d]'

NPM_PACKAGES="${HOME}/.npm-packages"
export PATH="$PATH:$NPM_PACKAGES/bin:${HOME}/.krew/bin:${HOME}/go/bin"

export DOCKER_BUILDKIT=1
export BUILDKIT_PROGRESS=plain

export WATCH_INTERVAL=1

HISTFILE="$HOME/.zsh_history"
export SAVEHIST=500000
export HISTSIZE=100000

# https://github.com/hkbakke/bash-insulter
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi

. /opt/asdf-vm/asdf.sh

source /usr/share/zsh/plugins/fzf-tab-git/fzf-tab.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

autoload -U +X bashcompinit && bashcompinit
autoload -Uz compinit && compinit
complete -o nospace -C /usr/bin/vault vault
source <(kubectl completion zsh)
compdef kubecolor=kubectl
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
