# Dotfiles

My Arch Linux dotfiles. The structure of files is the same as in the real home folder.

![Plasma With i3-gaps Screenshot](/images/plasma-i3-desktop.jpg "Plasma with i3-gaps")

## KDE Plasma 5 + i3-gaps

Installation procedures are described in the [KDE Plasma With i3 WM Instead of KWin](https://maxnatt.gitlab.io/posts/kde-plasma-with-i3wm/) article.

## License

Project is licensed under the Do What The Fuck You Want public license (for terms and conditions see `LICENSE` file or visit [http://www.wtfpl.net/txt/copying/](http://www.wtfpl.net/txt/copying/)) unless where otherwise stated.

## Credits

* [avivace](https://github.com/avivace/dotfiles) — Inspiration, i3wm + Plasma 5 configs
* [budRich](https://github.com/budRich/dots) — i3wm config
* [Airblader](https://github.com/Airblader/dotfiles-manjaro) — i3wm config
* [lahwaacz](https://github.com/lahwaacz/Scripts) — cache cleaning script
